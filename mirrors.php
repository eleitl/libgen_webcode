<?php
$mirrors_arr = array(
	0 => array(
		'mirror_link' => '/get.php?md5=' . strtoupper($row['MD5']) . $openreq,
		'mirror_title' => 'local',
		'mirror_tooltip' => 'local',
		'mirror_max_id' => 0,
		'main' => 0
	),
	1 => array(
		'mirror_link' => 'http://93.174.95.29/_ads/' . strtoupper($row['MD5']) . $openreq,
		'mirror_title' => 'Gen.lib.rus.ec',
		'mirror_tooltip' => 'Gen.lib.rus.ec',
		'mirror_max_id' => 9999999,
		'main' => 1
	),
	2 => array(
		'mirror_link' => 'http://185.39.10.101/ads.php?md5=' . strtoupper($row['MD5']) . $openreq,
		'mirror_title' => 'Libgen.lc',
		'mirror_tooltip' => 'Libgen.lc',
		'mirror_max_id' => 9999999,
		'main' => 0
	),
	3 => array(
		'mirror_link' => 'http://b-ok.cc/md5/' . strtoupper($row['MD5']),
		'mirror_title' => 'Z-Library',
		'mirror_tooltip' => 'Z-Library',
		'mirror_max_id' => 2396999,
		'main' => 0
	),
	4 => array(
		'mirror_link' => 'http://libgen.me/item/detail/id/' . $row['ID'],
		'mirror_title' => 'Libgen.me',
		'mirror_tooltip' => 'Libgen.me',
		'mirror_max_id' => 2289999,
		'main' => 0
	),
	5 => array(
		'mirror_link' => 'http://bookfi.net/md5/' . strtoupper($row['MD5']),
		'mirror_title' => 'BookFI.net',
		'mirror_tooltip' => 'BookFI.net',
		'mirror_max_id' => 1584000,
		'main' => 0
	),
);

$mirror_edit_link = 'https://libgen.is/librarian/registration.php?md5=' . strtoupper($row['MD5']);
$mirror_edit_title = 'Libgen Librarian';
$mirror_edit_tooltip = 'Libgen Librarian';

if($row['eDonkey'] == '')
{
	$mirror_e2k_link = '#';
	$mirror_e2k_title = '<font color="grey">Ed2k</font>';
	$mirror_e2k_tooltip = 'Ed2k';
}
else
{
	$mirror_e2k_link = 'ed2k://|file|' . strtoupper($row['MD5']) . '.' . $row['Extension'] . '|' . $row['Filesize'] . '|' . $row['eDonkey'] . '|h=' . $row['AICH'] . '|/';
	$mirror_e2k_title = 'Ed2k';
	$mirror_e2k_tooltip = 'Ed2k';
}

if($row['TTH'] == '')
{
	$mirror_dc_link = '#';
	$mirror_dc_title = '<font color="grey">DC++</font>';
	$mirror_dc_tooltip = 'DC++';
}
else
{
	$mirror_dc_link = 'magnet:?xt=urn:tree:tiger:' . $row['TTH'] . '&xl=' . $row['Filesize'] . '&dn=' . $row['MD5'] . '.' . $row['Extension'];
	$mirror_dc_title = 'DC++';
	$mirror_dc_tooltip = 'DC++';
}

if($row['torrent'] == '') 
{
	$mirror_oftorrent_link = '#';
	$mirror_oftorrent_title = '<font color="grey">'.$LANG_MESS_416.'</font>';
	$mirror_oftorrent_tooltip = str_replace('<br>', '', $LANG_MESS_416);
}
else
{
	$mirror_oftorrent_link = '/book/index.php?md5='.$row['MD5'].'&oftorrent=';
	$mirror_oftorrent_title = $LANG_MESS_416;
	$mirror_oftorrent_tooltip = str_replace('<br>', '', $LANG_MESS_416);
}

if($row['SHA1'] == '')
{
	$mirror_gnu_link = '#';
	$mirror_gnu_title = '<font color="grey">Gnutella</font>';
	$mirror_gnu_tooltip = 'Magnet';
}
else
{
	$mirror_gnu_link = 'magnet:?xt=urn:sha1:' .  $row['SHA1'] . '&xl=' . $row['Filesize'] . '&dn=' . $row['MD5'] . '.' . $row['Extension'];
	$mirror_gnu_title = 'Gnutella';
	$mirror_gnu_tooltip = 'Gnutella';
}
?>
