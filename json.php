<?php
require 'connect.php';

if (isset($_REQUEST['limit1']) && preg_match('|^\d+$|', $_REQUEST['limit1']))
	$limit1 = $_REQUEST['limit1'];
else
	$limit1 = '1000';

if (isset($_REQUEST['limit2']) && preg_match('|^\d+$|', $_REQUEST['limit2']))
	$limit2 = $_REQUEST['limit2'];
else
	$limit2 = '';

if ($limit2 == '')
	$limit = ' LIMIT ' . $limit1;
else
	$limit = ' LIMIT ' . $limit1 . ', ' . $limit2;

if (isset($_REQUEST['timefirst']))
	$timefirst = rawurldecode($_REQUEST['timefirst']);
else
	$timefirst = '';

if (isset($_REQUEST['timelast']))
	$timelast = rawurldecode($_REQUEST['timelast']);
else
	$timelast = '';

if (isset($_REQUEST['mode']) && in_array($_REQUEST['mode'], array('last', 'modified', 'newer')))
	$mode = $_REQUEST['mode'];
else
	$mode = '';

if (isset($_REQUEST['fields']))
{
	$fields_map = array(
		'id' => 'u.`id`',
		'title' => 'u.`title`',
		'volumeinfo' => 'u.`volumeinfo`',
		'series' => 'u.`series`',
		'periodical' => 'u.`periodical`',
		'author' => 'u.`author`',
		'year' => 'u.`year`',
		'edition' => 'u.`edition`',
		'publisher' => 'u.`publisher`',
		'city' => 'u.`city`',
		'pages' => 'u.`pages`',
		'language' => 'u.`language`',
		'topic' => 'u.`topic`',
		'library' => 'u.`library`',
		'issue' => 'u.`issue`',
		'identifier' => 'u.`identifier`',
		'issn' => 'u.`issn`',
		'asin' => 'u.`asin`',
		'udc' => 'u.`udc`',
		'lbc' => 'u.`lbc`',
		'ddc' => 'u.`ddc`',
		'lcc' => 'u.`lcc`',
		'doi' => 'u.`doi`',
		'googlebookid' => 'u.`googlebookid`',
		'openlibraryid' => 'u.`openlibraryid`',
		'commentary' => 'u.`commentary`',
		'dpi' => 'u.`dpi`',
		'color' => 'u.`color`',
		'cleaned' => 'u.`cleaned`',
		'orientation' => 'u.`orientation`',
		'paginated' => 'u.`paginated`',
		'scanned' => 'u.`scanned`',
		'bookmarked' => 'u.`bookmarked`',
		'searchable' => 'u.`searchable`',
		'filesize' => 'u.`filesize`',
		'extension' => 'u.`extension`',
		'md5' => 'u.`md5`',
		'generic' => 'u.`generic`',
		'visible' => 'u.`visible`',
		'locator' => 'u.`locator`',
		'local' => 'u.`local`',
		'timeadded' => 'u.`timeadded`',
		'timelastmodified' => 'u.`timelastmodified`',
		'coverurl' => 'u.`coverurl`',
		'identifierwodash' => 'u.`identifierwodash`',
		'tags' => 'u.`tags`',
		'pagesinfile' => 'u.`pagesinfile`',
		'descr' => 'd.`descr`',
		'toc' => 'd.`toc`',
		'sha1' => 'h.`sha1`',
		'sha256' => 'h.`sha256`',
		'crc32' => 'h.`crc32`',
		'edonkey' => 'h.`edonkey`',
		'aich' => 'h.`aich`',
		'tth' => 'h.`tth`',
		'btih' => 'h.`btih`',
		'torrent' => 'h.`torrent`',
	);
	$fields = strtolower(rawurldecode(rawurldecode($_REQUEST['fields'])));
	if ($fields == '*')
		$fields = array_values($fields_map);
	else
	{
		$fieldsarray = explode(',', preg_replace('|\s+|', '', $fields));
		$fields = array();
		$errors = array();
		foreach ($fieldsarray as $field)
		{
			if (array_key_exists($field, $fields_map))
				$fields[] = $fields_map[$field];
			else
				$errors[] = "\"$field\" is an invalid field name";
		}
		if ($errors)
			bad_request(implode("\n", $errors));
	}
}
else
	bad_request('"fields" parameter is missing');

if (isset($_REQUEST['isbn']) && !empty($_REQUEST['isbn']))
	$isbn = $_REQUEST['isbn'];
elseif (isset($_REQUEST['doi']) && !empty($_REQUEST['doi']))
	$doi = rawurldecode($_REQUEST['doi']);
elseif (!isset($_REQUEST['ids']) && $mode == '')
	bad_request('"ids" parameter is missing');

if (isset($_REQUEST['timenewer']) && $mode == 'newer')
{
	if (preg_match('|^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$|', rawurldecode($_REQUEST['timenewer'])))
	{
		$timenewer = rawurldecode($_REQUEST['timenewer']);
		if (isset($_REQUEST['idnewer']))
		{
			if (preg_match('|^\d+$|', $_REQUEST['idnewer']))
				$idnewer = $_REQUEST['idnewer'];
			else
				bad_request('invalid "idnewer" parameter value');
		}
		else
			$idnewer = '';
	}
	else
		bad_request('invalid "timenewer" parameter value');
}
elseif (!isset($_REQUEST['timenewer']) && $mode == 'newer')
	bad_request('"idnewer" parameter is missing');

define('DATE_REGEXP', '|^\d{4}-\d{2}-\d{2}$|');

if ($mode == 'last')
{
	if ($timefirst != '' && $timelast == '')
		$timelast = date('Y-m-d');
	if (preg_match(DATE_REGEXP, $timefirst) && preg_match(DATE_REGEXP, $timelast))
		$where = "(u.`TimeAdded` BETWEEN STR_TO_DATE('" . $timefirst . " 00:00:00','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('" . $timelast . " 23:59:59','%Y-%m-%d %H:%i:%s'))";
	$orderby = '';
}
else if ($mode == 'modified')
{
	if ($timefirst != '' && $timelast == '')
		$timelast = date('Y-m-d');
	if (preg_match(DATE_REGEXP, $timefirst) && preg_match(DATE_REGEXP, $timelast))
		$where = "(u.`TimeLastModified` BETWEEN STR_TO_DATE('" . $timefirst . " 00:00:00','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('" . $timelast . " 23:59:59','%Y-%m-%d %H:%i:%s'))";
	$orderby = '';
}
elseif ($mode == 'newer')
{
	if ($idnewer == '')
		$where = "(u.`TimeLastModified` > '" . mysql_real_escape_string($timenewer) . "')";
	else
		$where = "(u.`TimeLastModified` = '" . mysql_real_escape_string($timenewer) . "' AND u.`ID` > '" . mysql_real_escape_string($idnewer) . "') OR (u.`TimeLastModified` > '" . mysql_real_escape_string($timenewer) . "')";
	$orderby = "ORDER BY u.`TimeLastModified`, u.`ID`";
}
elseif (isset($doi))
{
	$where = "u.`DOI` = '" . mysql_real_escape_string($doi) . "'";
	$orderby = "ORDER BY u.`TimeLastModified`, u.`ID`";
}
elseif (isset($isbn))
{
	$isbn = preg_replace('/[[:punct:]]+/u', ' ', str_replace('-', '', $isbn));
	$isbn = preg_replace('/[\s]+/u', ' ', trim($isbn));
	$where = "MATCH(`IdentifierWODash`) AGAINST (' +" . str_replace(' ', ' +', $isbn) . "' IN BOOLEAN MODE)";
	$orderby = "ORDER BY u.`TimeLastModified`, u.`ID`";
}
else
{
	$ids = array();
	$hashes = array();
	foreach (explode(',', preg_replace('|\s+|', '', $_REQUEST['ids'])) as $id)
	{
		if (preg_match('|^\d+$|', $id))
			$ids[] = $id;
		else if (preg_match('|^[0-9a-fA-F]{32}$|', $id))
			$hashes[] = "'$id'";
	}
	$where = array();
	if ($ids)
		$where[] = 'u.`ID` IN (' . implode(',', $ids) . ')';
	if ($hashes)
		$where[] = 'u.`MD5` IN (' . implode(',', $hashes) . ')';
	$where = implode(' OR ', $where);
	$orderby = "ORDER BY u.`TimeLastModified`, u.`ID`";
	unset($ids, $hashes);
}

$sql = "SELECT " . implode(',', $fields) . " FROM `updated` AS `u` LEFT JOIN `hashes` AS `h` ON h.`md5` = u.`md5` LEFT JOIN `description` AS `d` ON d.`md5` = u.`md5` WHERE " . $where . " " . $orderby . " " . $limit;
mysql_query("SET time_zone='+00:00'", $con);
if (($result = mysql_query($sql, $con)) === FALSE)
{
	error_log(mysql_error($con));
	http_response_code(500);
	exit();
}
$rows = array();
while ($row = mysql_fetch_assoc($result))
	$rows[] = $row;
header('Content-Type: application/json; charset=UTF-8');
echo json_encode($rows, JSON_UNESCAPED_UNICODE);

function bad_request($message)
{
	echo $message;
	http_response_code(400);
	exit();
}
