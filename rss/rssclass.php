<?php
class RSS
{
	const feed_title = 'Library Genesis: last added';
	const feed_description = 'Last added files in the main collection of Library Genesis';

	public function GetFeed()
	{
		global $dbh, $dbtable, $servername;

		$where = array(
			"DATE_SUB('" . date('Y-m-d') . "', INTERVAL 7 DAY) <= `TimeAdded`",
			"`Visible` = ''"
		);

		if (isset($_GET['topicid']) && preg_match('|^[0-9]{1,3}$|', $_GET['topicid']))
		{
			$topic = $_GET['topicid'];
		//	$where[] = "`topic` IN (SELECT `topic_id` FROM `topics` WHERE (`topic_id` = $topic OR `topic_id_hl` = $topic) AND `lang` = 'en')";
			$where[] = "`topic` IN (SELECT `topic_id` FROM `topics` WHERE `topic_id` = $topic OR `topic_id_hl` = $topic GROUP BY `topic_id`)";
		}

		if (isset($_GET['language']))
			$where[] = "`language` = '" . mysql_real_escape_string($_GET['language']) . "'";

		$result = mysql_query("SELECT COUNT(*) FROM `" . $dbtable . "` WHERE " . implode(' AND ', $where), $dbh);
		if ($result === FALSE || ($row = mysql_fetch_row($result)) === FALSE)
		{
			error_log(mysql_error($dbh));
			http_response_code(500);
			exit();
		}
		if (isset($_GET['page']) && preg_match('/^\d+$/', $_GET['page']))
			$page = $_GET['page'];
		else
			$page = 1;
		$total_pages = ceil($row[0] / RSS_FEED_ITEMS);
		$prev_page = 0;
		$next_page = 0;
		if ($total_pages)
		{
			if ($page == 1 && $total_pages > 1)
				$next_page = $page + 1;
			else if ($page == $total_pages)
				$prev_page = $page - 1;
			else if ($page > $total_pages)
			{
				http_response_code(400);
				exit();
			}
			else
			{
				$prev_page = $page - 1;
				$next_page = $page + 1;
			}
		}

		$result = mysql_query("SELECT `ID`, `Title`, `Author`, `VolumeInfo`, `Publisher`, `Year`, `Pages`, `Periodical`, `Series`, `Language`, `Identifier`, `Edition`, `Extension`, `CoverURL`, `Filesize`, `MD5`, `TimeAdded` FROM `$dbtable` WHERE " . implode(' AND ', $where) . " ORDER BY `ID` DESC LIMIT " . ($page - 1) * RSS_FEED_ITEMS . "," . RSS_FEED_ITEMS, $dbh);
		if ($result === FALSE)
		{
			error_log(mysql_error($dbh));
			http_response_code(500);
			exit();
		}
		$feed_url = 'http://' . $servername . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH) . '?';
		$feed_url_args = isset($topic) ? 'topicid=' . $topic : '';
		$feed = '<?xml version="1.0" encoding="UTF-8"?>'
			. '<rss version="2.0">'
			. '<channel>'
			. '<title>' . htmlspecialchars(self::feed_title) . '</title>'
			. '<description>' . htmlspecialchars(self::feed_description) . '</description>'
			. '<link>http://' . $servername . '/</link>'
			. ($prev_page ? '<link rel="prev" href="' . $feed_url . $feed_url_args . '&amp;page=' . $prev_page. '"/>' : '')
			. ($next_page ? '<link rel="next" href="' . $feed_url . $feed_url_args . '&amp;page=' . $next_page. '"/>' : '');
		while ($row = mysql_fetch_array($result))
		{
			$title = htmlspecialchars(trim($row['Title']), ENT_QUOTES);
			$author = htmlspecialchars(trim($row['Author']), ENT_QUOTES);
			$vol = htmlspecialchars(trim($row['VolumeInfo']), ENT_QUOTES);
			$publisher = htmlspecialchars(trim($row['Publisher']), ENT_QUOTES);
			$year = htmlspecialchars(trim($row['Year']), ENT_QUOTES);
			$pages = htmlspecialchars(trim($row['Pages']), ENT_QUOTES);
			$periodical = htmlspecialchars(trim($row['Periodical']), ENT_QUOTES);
			$series = htmlspecialchars(trim($row['Series']), ENT_QUOTES);
			$lang = htmlspecialchars(trim($row['Language']), ENT_QUOTES);
			$isbn = htmlspecialchars(trim($row['Identifier']), ENT_QUOTES);
			$edition = htmlspecialchars(trim($row['Edition']), ENT_QUOTES);
			$ext = strtoupper($row['Extension']);
			$coverurl = htmlspecialchars(trim($row['CoverURL']), ENT_QUOTES);
			if ($coverurl == '')
				$coverurl = '/img/blank.png';
			elseif (strpos($coverurl, '://') === FALSE)
				$coverurl = '/covers/' . $coverurl;

			if ($periodical != '')
			{
				$booknameperiod1 = '[' . $periodical . '] ';
				$booknameperiod2 = '<font color="gray"><i>' . $periodical . '</i></font> ';
			}
			else
			{
				$booknameperiod1 = '';
				$booknameperiod2 = '';
			}

			if ($series != '')
			{
				$booknameseries1 = '(' . $series . ') ';
				$booknameseries2 = '<font color="green"><i>(' . $series . ')</i></font> ';
			} 
			else
			{
				$booknameseries1 = '';
				$booknameseries2 = '';
			}

			$size = $row['Filesize'];
			if ($size >= 1024 * 1024 * 1024) {
				$size = round($size / 1024 / 1024 / 1024);
				$size = $size . ' GB';
			} else if ($size >= 1024 * 1024) {
				$size = round($size / 1024 / 1024);
				$size = $size . ' MB';
			} else if ($size >= 1024) {
				$size = round($size / 1024);
				$size = $size . ' kB';
			}
			else
				$size = $size . ' B';

			// book info section (in parentheses)
			$volinf = $publisher;
			if ($year)
			{
				if ($volinf)
					$volinf = $volinf . ', ' . $year;
				else
					$volinf = $year;
			}
			if ($pages)
			{
				if ($volinf)
					$volinf = $volinf . ', ' . $pages . ' pp.';
				else
					$volinf = $pages . ' pp.';
			}
			$volstamp = '';
			if ($volinf)
				$volstamp = ' <font face="Times" color="green"><i>(' . $volinf . ')</i></font>';

			$vol_ed = $vol;
			if ($edition)
			{
				if ($vol_ed)
					$vol_ed = $vol_ed . ', ' . $edition . ' ed.';
				else
					$vol_ed = $edition . ' ed.';
			}
			$volume = '';
			if ($vol_ed)
				$volume = ' <font face="Times" color="green"><i>[' . $vol_ed . ']</i></font>';

			$link = htmlspecialchars('http://' . $servername . '/book/index.php?md5=' . $row['MD5']);
			$feed .= '<item>'
				. '<guid isPermaLink="false">' . strtoupper($row['MD5']) . '</guid>'
				. '<title>' . $booknameperiod1 . $booknameseries1 . $title . '</title>'
				. '<link>' . $link . '</link>'
				. '<description>' . htmlspecialchars(
					'<table border="0">'
					. '<tr><td rowspan="9"><a href="' . $link . '"><img src="' . $coverurl . '" width="120"></a></td>'
					. '<td valign="top" colspan="2">' . $author . '. <b>' . $booknameperiod2 . $booknameseries2 . $title . '</b>' . $volume . $volstamp.'</td></tr>'
					. '<tr><td valign="top" width="160"><font color="grey">Author:</font></td><td>' . $author . '</td></tr>'
					. '<tr><td valign="top"><font color="grey">ISBN:</font></td><td>' . $isbn . '</td></tr>'
					. '<tr><td valign="top"><font color="grey">Size:</font></td><td>' . $size . ' [' . $ext . ']</td></tr>'
					. '<tr><td valign="top"><font color="grey">Periodical:</font></td><td>' . $periodical . '</td></tr>'
					. '<tr><td valign="top"><font color="grey">Series:</font></td><td>' . $series . '</td></tr>'
					. '<tr><td valign="top"><font color="grey">Language:</font></td><td>' . $lang . '</td></tr>'
					. '<tr><td valign="top"><font color="grey">ID:</font></td><td>' . $row['ID'] . '</td></tr>'
					. '<tr><td valign="top"><font color="grey">Date Added:</font></td><td>' . $row['TimeAdded'] . '</td></tr>'
					. '</table>')
				. '</description>'
				. '</item>';
		}
		$feed .= '</channel></rss>';
		return $feed;
	}
}
