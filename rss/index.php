<?php
require '../config.php';
require 'rssclass.php';

$dbh = mysql_connect(DB_HOST, DB_USERNAME, DB_PASSWORD);
if (!$dbh)
{
	$err = mysql_error();
	error_log($err);
	echo $htmlhead . "<font color='#A00000'><h1>Error</h1></font>Could not connect to the database: " . htmlspecialchars($err) . "<br>Cannot proceed." . $htmlfoot;
	http_response_code(500);
	exit();
}
mysql_query("SET NAMES 'utf8'");
mysql_select_db(DB_NAME, $dbh);

$rss = new RSS();
header("Content-Type: text/xml; charset=utf-8");
echo $rss->GetFeed();
