<?php
include_once('config.php');

$doi = $_GET['doi'];

$sqlbibtex = "SELECT 
s.doi,
s.title, 
s.author,
m.publisher,
m.magazine,
m.issnp,
m.issne,
s.isbn,
s.year,
s.month,           
s.day,
s.volume, 
s.issue,
s.first_page, 
s.last_page,
s.journalid
FROM `scimag` s left join `magazines` m on s.journalid = m.journalid  
WHERE `DOI`='".mysql_real_escape_string($doi)."'";


$resultbibtex = mysql_query($sqlbibtex);
$rowbibtex = mysql_fetch_assoc($resultbibtex);



$doi = $rowbibtex['doi'];
$title = strip_tags($rowbibtex['title']);
$author = strip_tags($rowbibtex['author']);
$publisher = strip_tags($rowbibtex['publisher']);
$magazine = strip_tags($rowbibtex['magazine']);
$issnp = $rowbibtex['issnp'];
$issne = $rowbibtex['issne'];
$isbn = $rowbibtex['isbn'];
$year = $rowbibtex['year'];
$month = $rowbibtex['month'];
$day = $rowbibtex['day'];
$volume = $rowbibtex['volume'];
$issue = $rowbibtex['issue'];
$first_page = $rowbibtex['first_page'];
$last_page = $rowbibtex['last_page'];
$journalid = $rowbibtex['journalid'];

$firstauth = substr($author, 0, stripos($author, ' ')-1);

if ($journalid == 'book'){
$typedoc = '@inbook';
}
elseif($journalid == 'conf'){
$typedoc = '@inproceedings';
}
else{
$typedoc = '@article';
}

$data = "<textarea rows='20' name='bibtext' id='bibtext' readonly cols='150'>$typedoc{ $firstauth-$year,
doi	= { $doi},
title	= { $title},
author	= { $author},
publisher	= { $publisher},
journal	= { $magazine},
issnp	= { $issnp},
issne	= { $issne},
isbn	= { $isbn},
year	= { $year},
month	= { $month},
day	= { $day},
volume	= { $volume},
issue	= { $issue},
page	= { $first_page--$last_page},
url =       {http://gen.lib.rus.ec/scimag/index.php?s=$doi},
abstract = { }
}</textarea>";

$data = explode("\r\n", $data);
$data = array_filter($data, create_function('$data','return !preg_match("#\{ \}#", $data);'));
$data = implode("\r\n", $data);


$data = strtr($data, array('{ ' => '{', ' }' => '}'));
echo $data;

echo $footer;
mysql_free_result($resultbibtex);
mysql_close($mysql);

?>