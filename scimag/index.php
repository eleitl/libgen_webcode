<?php
ob_start(); //сбрасывает уже переданный хедер, чтоб работал хедер при переадресации если не найдено ничего, для этого же ob_flush в конце

include_once('config.php');
include_once 'menu_'.$lang.'.html';
echo $libgensalogo;

$items_on_page = 25;


if (isset($_GET['redirect']))
{
	$redirect = trim($_GET['redirect']);
}
else
{
	$redirect = '0';
}

if (isset($_GET['s']))
{
	$s = trim($_GET['s']);
}
else
{
	$s = '';
}
if (isset($_GET['page']))
{
	$page = trim($_GET['page']);
}
else
{
	$page = 1;
}
if (isset($_GET['open']))
{
	$open = trim($_GET['open']);
}
else
{
	$open = '0';
}
if (isset($_GET['v']))
{
	$volumesearch = trim($_GET['v']);
}
else
{
	$volumesearch = '';
}
if (isset($_GET['i']))
{
	$issuesearch = trim($_GET['i']);
}
else
{
	$issuesearch = '';
}
if (isset($_GET['journalid']))
{
	$journalid = trim($_GET['journalid']);
}
else
{
	$journalid = '';
}

if (isset($_GET['p']))
{
	$articlepage = trim($_GET['p']);
}
else
{
	$articlepage = '';
}

echo '<form name="search">' . "\n";
echo '<table width=900 align=center>' . "\n";
echo '<tr><td><input  autofocus type=edit size=70 maxlength=200 name="s" value="' . $s . '"></td>
<td><input type=edit size=15 name="journalid" value="' . $journalid . '"></td>
<td>&gt;</td><td><input type=edit size=10 name="v" value="' . $volumesearch . '"></td>
<td>&gt;</td><td><input type=edit size=10 name="i" value="' . $issuesearch . '"></td>
<td>&gt;</td><td><input type=edit size=5 name="p" value="' . $articlepage . '"></td>
<td><input type="submit" value="' . $LANG_SEARCH_0 . '" onclick="this.disabled="disabled"; document.forms.item(0).submit();"><br></td></tr>

<tr valign=top><td><font face=Arial color=gray size=1>' . $LANG_MESS_18 . ' <a href="http://sci-hub.tw/">Sci-Hub</a></font></td>
<td><font face=Arial color=gray size=1>' . $LANG_MESS_55 . '</font></td>
<td></td><td><font face=Arial color=gray size=1>' . $LANG_MESS_118 . '</font></td>
<td></td><td><font face=Arial color=gray size=1>' . $LANG_MESS_56 . '</font></td>
<td></td><td><font face=Arial color=gray size=1>' . $LANG_MESS_28 . '</font></td>
<td><input type=checkbox name="redirect" value="1" checked><font size=1 face=Arial color=gray>Redirect</font></td>
</tr>
';
echo '</table>' . "\n";
echo '</form>' . "\n";
$s = $s;
$s = preg_replace('/[\s]+/u', ' ', $s);
$s = trim($s);
$s = htmlspecialchars_decode($s);




//анализируем journalid - journalid, issn, magnaim, и в зав. от этого поставляем в поисковый запрос 


if ($journalid!='')
{
	if($journalid =='book' || $journalid =='conf')
	{
		$journalidm = $journalid;	
	}

	else
	{
		if(preg_match('~^[0-9]{0,5}$~', $journalid))
		{
			$sqlmagjournalid = "SELECT `JOURNALID` FROM `magazines` where `JOURNALID` =  '".mysql_real_escape_string($journalid)."' LIMIT 1";
		}
		elseif (preg_match('|^[0-9]{4}\-[0-9X-x]{4}$|', $journalid)) //ищем по ISSN
		{
			$sqlmagjournalid = "SELECT `JOURNALID` FROM `magazines` where `issnp` = '".mysql_real_escape_string($journalid)."' or `issne` =  '".mysql_real_escape_string($journalid)."' LIMIT 1";
		}
		else
		{
			$sqlmagjournalid = "SELECT `JOURNALID` FROM `magazines` where upper(`magazine`) LIKE upper('" . str_replace(' ', '% ', mysql_real_escape_string($journalid)) . "%') order by magazine LIMIT 1";
		}
		//echo $sqlmagjournalid;
		$resmagjournalid = mysql_query($sqlmagjournalid);
		if (mysql_num_rows($resmagjournalid) != 0)
		{
			$rowmagjournalid = mysql_fetch_assoc($resmagjournalid);
			$journalidm      = $rowmagjournalid['JOURNALID'];
		}
		else
		{
			$journalidm      = 'journalidnotfound';	
		}
	}
}

if ($s != '' || isset($_GET['last']) || $_GET['md5']!='' || $journalid != '')
{

	if ($s != '')
	{
		if (preg_match('(^10\.\d{4,5}/[\d\:\.\,\(\)\;\[\]\_\<\>\&\-\+\/\\a-zA-Z]{1,200}$)', $s) )
		{

			$sql_world = " (`DOI`='".mysql_real_escape_string($s)."' OR `DOI2`='".mysql_real_escape_string($s)."') ";

		}
		elseif(preg_match('(^[0-9]{1,8}$)', $s))
		{
			$sql_world = " ( `pubmedid`='".mysql_real_escape_string($s)."' OR `DOI`='10.0000/PMID".mysql_real_escape_string($s)."' ) ";
		
		}
		else
		{
			$s     = preg_replace('/[[:punct:]]+/u', ' ', $s);
			$s     = preg_replace('/[\s]+/u', ' ', $s);
			$words = explode(" ", $s);
			for ($i = 0, $c = count($words); $i < $c; $i++)
			{
				if (mb_strlen($words[$i], 'UTF-8') <= 3) //убираем все что короче букв
					unset($words[$i]);
			}
			$words = array_values($words);
			$words = array_slice($words, 0, 20); //вырезаем все что короче 3 символов и берем не более 6 слов
			$w = array();
			$words = implode(' ', $words);
			$sql_world = "  MATCH(`Title`,`Author`) AGAINST('» +".str_replace(' ', ' +', $words)."»' IN BOOLEAN MODE) "; // » - полностью по фразе
		}

	}


	if ($journalid != '')
	{
		$sql_world_journal = ' ';
		$sql_world_journal .= " `JOURNALID`='".mysql_real_escape_string($journalidm)."'";

		if ($volumesearch != '')
		{
			if (preg_match('~^(16|17|18|19|20)[0-9]{2}$~', $volumesearch))
			{
				$sql_world_journal .= " AND `year`= '".mysql_real_escape_string($volumesearch)."'";
				if ($issuesearch != '')
				{
					$sql_world_journal .= " AND `issue`= '".mysql_real_escape_string($issuesearch)."'";
					if ($articlepage != '')
					{
						$sql_world_journal .= " AND `first_page`='".mysql_real_escape_string($articlepage)."'";
					}
				}
			}
			else
			{
				$sql_world_journal .= " AND `volume`= '".mysql_real_escape_string($volumesearch)."'";
				if ($issuesearch != '')
				{
					$sql_world_journal .= " AND `issue`= '".mysql_real_escape_string($issuesearch)."'";
					if ($articlepage != '')
					{
						$sql_world_journal .= " AND `first_page`='".mysql_real_escape_string($articlepage)."'";
					}
				}
				else
				{
					if ($articlepage != '') //если ищем только том и страницу в томе (для случаев когда есть сквозная нумерация в томе)
					{
						$sql_world_journal .= " AND `first_page`='".mysql_real_escape_string($articlepage)."'";
					}
				}
			}
		}

		if($s!='')
		$sql_world .= '  AND ( ' .$sql_world_journal.') ';
		else
		$sql_world = $sql_world_journal;
	}
	//выводим последнее
	if (isset($_GET['last']))
		$sql = "SELECT *  FROM `scimag` ORDER BY ID DESC LIMIT 2500";
	elseif (isset($_GET['md5']) && preg_match('~^[0-9A-Za-z]{32}$~', $_GET['md5']))
		$sql = "SELECT * FROM `scimag` WHERE MATCH(`md5`) AGAINST ('".$_GET['md5']."') ";
	elseif (preg_match('~`DOI`=~', $sql_world))
		$sql = str_replace(' WHERE AND ', ' WHERE ', "select * from `scimag` WHERE " . $sql_world . " ");
	else
		$sql = str_replace(' WHERE AND ', ' WHERE ', "SELECT * FROM `scimag` WHERE " . $sql_world . " LIMIT 100");

	$res = mysql_query($sql);
	//если ничего не найдено, и заполнен journalid - возможно journalid - название журнала, ищем journalid по нему
	if ($res === FALSE)
	{
		error_log(mysql_error($mysql));
		http_response_code(500);
		exit();
	}
	
	$cn  = mysql_num_rows($res);
	//if($cn != 0)
	//{
	//	echo $LANG_MESS_77.' '.$LANG_MESS_170.': '.$cn;
	//}

	if ($cn == 100 || $cn == 2500)
		echo '<font color=grey size=1>' . $LANG_MESS_232 . '  ' . $cn . ' '. $LANG_MESS_190 .' </font>';
	else
		echo '<font color=grey size=1>' . $LANG_MESS_77 . ' ' . $cn . ' '. $LANG_MESS_190 .' </font>';

	header("LG-PAPERS-FOUND: $cn"); // выводим в http заголовок кол-во найденного для скайхаба.
	if ($cn == 0 && preg_match('(^10\.\d{4,5}/[\d\:\.\(\)\;\[\]\_\<\>\+\-\/\\a-zA-Z]{1,100}$)', $s) && $redirect == '1')
	{
		header('Location: http://dx.doi.org.sci-hub.tw/' . $s, true, 301);
	}
	elseif ($cn == 0 && !preg_match('(^10\.\d{4,5}/[\d\:\.\(\)\;\[\]\_\<\>\+\-\/a-zA-Z]{1,100}$)', $s) && !isset($_GET['last']) && $redirect == '1')
	{
		header('Location: http://scholar.google.com.sci-hub.tw/scholar?q='.$s, true, 301 );
	}
	elseif($cn == 0  && $redirect != '1' && preg_match('(^10\.\d{4,5}/[\d\:\.\(\)\;\[\]\_\<\>\+\-\/a-zA-Z]{1,100}$)', $s))
	{
		die("<table width=100%><tr><td align=center>".$LANG_MESS_181."<a href='http://dx.doi.org.sci-hub.tw/".$s."'>Sci-Hub?</a></td></tr></table>");
	}
	elseif($cn == 0  && $redirect != '1' && !preg_match('(^10\.\d{4,5}/[\d\:\.\(\)\;\[\]\_\<\>\+\-\/a-zA-Z]{1,100}$)', $s))
	{
		die("<table width=100%><tr><td align=center><b>".$LANG_MESS_181."<a href='http://scholar.google.com.sci-hub.tw/scholar?q=".$s."'>Google Scholar?</a></td></tr></table>");
	}

	$pages = ceil($cn / $items_on_page);
	$nav   = "";
	if (isset($_GET['last']))
		$nav .= 'index.php?last=&page=';
	else
		$nav .= 'index.php?s=' . $s . '&journalid=' . $journalid . '&v=' . $volumesearch . '&i=' . $issuesearch .'&p='.$articlepage.'&page=';
	if (isset($pages)) 
	{
		if ($pages > 1) 
		{
			$to_page = ($page * 25); if($to_page > $cn) $to_page = $cn;
			echo '<font color=grey size=1>| ' . $LANG_MESS_231 . ' ' .(($page * 25) -24 ) . ' '.$LANG_MESS_63.' ' .$to_page. '</font>';
			echo  '<div style="text-align: center;" class="paginator" id="paginator_example_top"></div>
			<script type="text/javascript">
			    paginator_example_top = new Paginator(
			        "paginator_example_top", // id контейнера, куда ляжет пагинатор
			        '.$pages.', // общее число страниц
			        25, // число страниц, видимых одновременно
			        '.$page.', // номер текущей страницы
			        "'.$nav.'" // url страниц
			    );
			</script>';
		} 
	}

	$i     = 0;
	$links = "";

	echo "<table width=1024 cellspacing=1 cellpadding=1 rules=rows align=center>"
		. "<thead><tr>"
		. "<td width=130><b>DOI</b></td>"
		. "<td width=200><b>" . $LANG_MESS_6 . "</b></td>"
		. "<td width=300><b>" . $LANG_MESS_57 . "</b></td>"
		. "<td width=100><b>" . $LANG_MESS_146 . "</b></td>"
		. "<td width=100><b>" . $LANG_MESS_58 . "</b></td>"
		. "<td width=60><b>" . $LANG_MESS_56 . "</b></td>"
		. "<td width=50><b>ISSN</b></td>"
		. "<td width=60><b>" . $LANG_MESS_60 . "</b></td>"
		. "<td width=50><b>" . $LANG_MESS_171 . "</b></td>"
		. "</tr></thead>";
	while ($row = mysql_fetch_assoc($res))
	{
		if ($i >= (($page - 1) * $items_on_page) and ($i < $page * $items_on_page))
		{
			$doi          = stripslashes($row['DOI']);
			$md5doi = md5($row['DOI']);
			$doipublisher = substr($doi, 0, strpos($doi, '/') );
			
			//10\.0000\/PMID фиктивный doi для поддержания формата базы и файлохранилища

			//доп. идентификаторы

			if(!preg_match('|^10\.0000\/PMID|', $doi))
			{
				$identifiers = '<tr><td><b>DOI:</td><td>'.$doi.'</b></td></tr>';
			}			
			if($row['PubmedID']!='')
			{
				$identifiers = $identifiers.'<tr><td><b>PMID:</td><td>'.$row['PubmedID'].'</b></td></tr>';
			}
			if($row['PII']!='')
			{
				$identifiers = $identifiers.'<tr><td><b>PII:</td><td>'.$row['PII'].'</b></td></tr>';
			}
			if($row['PMC']!='')
			{
				$identifiers = $identifiers.'<tr><td><b>PMC:</td><td>'.$row['PMC'].'</b></td></tr>';
			}
			if($row['DOI2']!='')
			{
				$identifiers = $identifiers.'<tr><td><b>DOI2:</td><td>'.$row['DOI2'].'</b></td></tr>';
			}
			

//зеркала
			if ($open == '1' )
			{
				$mirror1_link = '<a href="http://libgen.io/scimag/ads.php?doi=' . rawurlencode($doi) . '&open=1&downloadname="  title="'.$row['TimeAdded'].' ID: '.$row['ID'].'"><b>Libgen</b></a>';
			}
			else
			{
				$mirror1_link = '<a href="http://libgen.io/scimag/ads.php?doi=' . rawurlencode($doi) . '&downloadname="  title="'.$row['TimeAdded'].' ID: '.$row['ID'].'">Libgen</a>';
			}
			$mirror2_link = '<a href="http://moscow.sci-hub.tw/' . md5(strtolower($doi)) . '/'.rawurlencode(strtr($doi, array('/'=> '@'))).'.pdf">Sci-Hub (Moscow)</a>';
			$mirror3_link = '<a href="http://ocean.sci-hub.tw/'  . md5(strtolower($doi)) . '/'.rawurlencode(strtr($doi, array('/'=> '@'))).'.pdf">Sci-Hub (Ocean)</a>';
			$mirror4_link = '<a href="http://cyber.sci-hub.tw/'  . base64_encode(strtolower($doi)) . '/'.rawurlencode(strtr($doi, array('/'=> '@'))).'.pdf">Sci-Hub (Cyber)</a>';
			$mirror5_link = '<a href="http://sci.libgen.pw/item/detail/' . htmlspecialchars($row['MD5']) . '">Libgen.pw</a>';
			$mirror6_link = '<a href="http://booksc.org/s/?q=' . rawurlencode($doi) . '&t=0">BookSC</a>';

			if(!isset($_GET['last']))//линк на торренты, если это не список последних поступлений, торрента как правило нет
			{
				$torrent_link = '<a href="' . TORRENTS_URL . 'sm_' .str_pad((ceil($row['ID']/100000)*100000 - 100000), 8,'0',STR_PAD_LEFT).'-'.str_pad((ceil($row['ID']/100000)*100000-1), 8,'0',STR_PAD_LEFT).'.torrent">Torrents</font></a>';
			}
			else
			{
				$torrent_link = '';
			}
			$author    = stripslashes($row['Author']);
			$article   = stripslashes($row['Title']);
			$journalid    = stripslashes($row['JOURNALID']);

			$abstracturl       = stripslashes($row['AbstractURL']);
			if(preg_match('~^http~', $abstracturl))
			{
				$abstracturl = "<br><a href='http://anonym.to/?".$abstracturl."'>Abstract</a>";
			}
			if(!preg_match('~^10\.0000~', $doi))
			{
				$doiorg = "<br><a href='http://anonym.to/?http://dx.doi.org/".rawurlencode($doi)."'>dx.doi.org</a>";
			}
			else
			{
				$doiorg = '';
			}
			$md5       = stripslashes($row['MD5']);
			$year      = stripslashes($row['Year']);
			$month     = stripslashes($row['Month']);
			$fpage     = stripslashes($row['First_page']);
			$lpage     = stripslashes($row['Last_page']);
			$day       = stripslashes($row['Day']);
			$volume    = trim(stripslashes($row['Volume']));
			$issue     = stripslashes($row['Issue']);
			//определяем изд. по doi
			$sqlpub    = 'select * from `publishers` where `DOICode`="' . $doipublisher . '"';
			$filesize  = ceil($row['Filesize'] / 1024);
			$publres   = mysql_query($sqlpub);
			$rowpubl   = mysql_fetch_assoc($publres);
			$publisher = stripslashes($rowpubl['Publisher']);
			//определяем журн. по спрингерлинк id или по Issnp, issne       
			if ($journalid != '')
			{



				if(!in_array($journalid, array('book','conf')))
				{

					$sqlmag   = 'select * from `magazines` where `JOURNALID`="' . $journalid . '"';
					//echo $sqlmag;
					$magres   = mysql_query($sqlmag);
					$rowmag   = mysql_fetch_assoc($magres);
					$magazine = stripslashes($rowmag['Magazine']);
					$issnp    = stripslashes($rowmag['ISSNP']);
					if($issnp !='')
					{
						$issnp = $issnp . '(p)';
					}
					$issne    = stripslashes($rowmag['ISSNE']);
					if($issne !='')
					{
						$issne = $issne . '(e)';
					}
					//if($lang!='ru')
					//{
					//	$magazine = $magazine;
					//}
					//else
					//{
			        		$magazine = "<a href='journaltable.php?journalid=".$journalid."'>$magazine</a>";
					//}
				}
				else
				{
					$magazine = $journalid;
					$issnp    = '';
					$issne    = '';	
				}
			}
			else
			{
			   	$magazine = '';
				$issnp    = '';
				$issne    = '';
			}

			$line = "<tr>"
				. "<td width=130>"
				. "<table>".$identifiers
				. "<tr><td><b>".$LANG_MESS_323.":</b></td><td>".$mirror1_link."<td></tr>"
				. "<tr><td></td><td>".$mirror2_link."<td></tr>"
				. "<tr><td></td><td>".$mirror3_link."<td></tr>"
				. "<tr><td></td><td>".$mirror4_link."<td></tr>"
				. "<tr><td></td><td>".$mirror5_link."</td><tr>"
				. "<tr><td></td><td>".$mirror6_link."</td><tr>"
				. "<tr><td></td><td>".$torrent_link."</td><tr>"
				. "</table></td>"
				. "<td width=200>$author</td>"
				. "<td width=300>$article</td>"
				. "<td width=100>$publisher</td>"
				. "<td width=100>$magazine</td>"
				. "<td width=60>" . $LANG_MESS_10 . ":$year<br>"
				. $LANG_MESS_66 . ":$month<br>"
				. $LANG_MESS_67 . ":$day<br>"
				. $LANG_MESS_42 . ":$volume<br>"
				. $LANG_MESS_56 . ":$issue<br>"
				. $LANG_MESS_64 . ":$fpage<br>"
				. $LANG_MESS_65 . ":$lpage</td>"
				. "<td width=50>$issnp<br>$issne</td>"
				. "<td width=60>$filesize$LANG_MESS_KB</td>"
				. "<td width=40><a href='' title='$md5'><b>MD5</b></a>"
				. "<br><a href='/scimag/errorreport.php?doi=$row[DOI]'><b>".$LANG_MESS_172."</b></a>"
				. "<br><a href='/scimag/librarian/form.php?doi=$row[DOI]'><b>".$LANG_MESS_30."</b></a>"
				. "<br><a href='bibtex.php?doi=$row[DOI]'><b>BibTeX</b></a>"
				. "<b>$doiorg$abstracturl</b>"
				. "</td>"
				. "</tr>";
			echo $line;
			unset($sqlmag);
			unset($magazine);
			unset($issnp);
			unset($issne);
			unset($author);
			unset($article);
			unset($month);
			unset($day);
			unset($issue);
			unset($fpage);
			unset($lpage);
			unset($year);
			unset($md5doi);
		}
		$i++;

	}
	echo "</table>";
	mysql_free_result($res);
}
if (isset($pages) && $pages > 1)
{
	echo  '<div style="text-align: center;" class="paginator" id="paginator_example_bottom"></div>
	<script type="text/javascript">
		paginator_example_bottom = new Paginator(
			"paginator_example_bottom", // id контейнера, куда ляжет пагинатор
			'.$pages.', // общее число страниц
			25, // число страниц, видимых одновременно
			'.$page.', // номер текущей страницы
			"'.$nav.'" // url страниц
		);
	</script>';
}
echo $footer;
mysql_close($mysql);
ob_flush();
?>
