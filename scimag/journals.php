<?php 
include_once('config.php');
include_once 'menu_'.$lang.'.html';
echo $libgensalogo;

if(!isset($_GET['letter']))
	die("<font color='#A00000'><h1>Wrong Letter</h1></font>");
$letterjournal = $_GET['letter'];

if(!preg_match('~^(0-9|[A-Z]{1}|Other)$~', $letterjournal))
	die("<font color='#A00000'><h1>Wrong Letter</h1></font>");

if ($letterjournal == '0-9')
	$like = "RLIKE '^[0-9]'";
elseif($letterjournal == 'Other')
	$like = "NOT RLIKE '^[0-9A-Za-z]'";
else
	$like = "LIKE '" . mysql_real_escape_string($letterjournal) . "%'";
if (($result = mysql_query("SELECT DISTINCT m.`journalid`, m.`magazine`, m.`issnp`, m.`issne` FROM `magazines` m WHERE m.`magazine` $like AND EXISTS (SELECT 1 FROM scimag WHERE scimag.journalid = m.journalid LIMIT 1)", $mysql)) === FALSE)
{
	error_log($_SERVER['REQUEST_URI'] . ': ' . mysql_error());
	http_response_code(500);
	exit();
}
echo "<table width=1000 cellspacing=1 cellpadding=1 rules=rows align=center>
<thead><tr>
<td width=700><b>".$LANG_MESS_5."</b></td>
<td width=100><b>ISSN ".$LANG_MESS_69."</b></td>
<td width=100><b>ISSN ".$LANG_MESS_70."</b></td>
 </tr></thead>";
while ($row = mysql_fetch_assoc($result)) 
{
	echo "<tr><td width=700><a href=\"journaltable.php?journalid={$row['journalid']}\">" . htmlspecialchars($row['magazine']) . "</a></td><td width=150>{$row['issnp']}</td><td width=150>{$row['issne']}</td></tr>";
}
echo "</table>";
echo $footer;
mysql_free_result($result);
mysql_close($mysql);
?>
