<?php
if (isset($_COOKIE['lang']) && preg_match('/^(en|ru)$/', $_COOKIE['lang'])) {
	$lang      = $_COOKIE['lang'];
	$lang_file = 'lang_' . $lang . '.php';
	if (!file_exists($lang_file))
		$lang_file = 'lang_en.php';
} else {
	$lang      = 'en';
	$lang_file = 'lang_en.php';
}

include('../lang_'.$lang.'.php');

$header = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
	<META HTTP-EQUIV='CACHE-CONTROL' CONTENT='NO-CACHE'>
	<meta name='robots' content='noindex,nofollow'>
	<meta name='rating' content='general'>
	<link rel='stylesheet' href='/menu.css' type='text/css' media='screen' />
	<title>Library Genesis: Scientific Articles</title>
	<!--[if IE 6]>
	<style>
		body {behavior: url('/csshover3.htc');}
		#menu li .drop {background:url('/img/drop.gif') no-repeat right 8px; 
	</style>
	<![endif]-->
</head>";
$footer = "</body></html>";
$libgensalogo = "<table width='100%'><tr><td colspan='2' align='center'><font color=#A00000><a href='/'><h1>Library Genesis</a>: <a href='index.php'>Scientific Articles</a> <sup><font size=4>lim<sub>n->∞</sub></font></sup></h1></font></td></tr><tr><td width='200px' valign='top'></td></tr></table>";

if (!isset($_GET['format']) || $_GET['format'] != 'json')
	echo $header;

define('DB_HOST', 'localhost');
define('DB_USERNAME', '');
define('DB_PASSWORD', '');
define('DB_NAME', '');

$mysql = mysql_connect(DB_HOST, DB_USERNAME, DB_PASSWORD);
if (!$mysql) {
	error_log(mysql_error());
	http_response_code(500);
	exit();
}
mysql_query("SET NAMES 'utf8'");
mysql_select_db(DB_NAME, $mysql);

define('TORRENTS_URL', 'http://gen.lib.rus.ec/scimag/repository_torrent/');
