<?php
require '../config.php';

class RSS
{
	public function GetFeed()
	{
		include 'strings.php';

		// pagination
		global $mysql,$dbtable,$maxnewslines,$pagesperpage,$servername;

		$where = "";

		$sql_cnt = "SELECT COUNT(*) FROM $dbtable  ";
//echo $sql_cnt;
		$result = mysql_query($sql_cnt, $mysql);
		if (!$result) die($dberr);
		$row = mysql_fetch_assoc($result);
		$count = stripslashes($row['COUNT(*)']);


		mysql_free_result($result);

		$pagestotal = ceil($count/$maxnewslines);
		if ($pagestotal <= 1) $pagestotal = 1;

		if (isset($_GET['page'])) $page = $_GET['page'];
		else $page = 1;

		$query = "SELECT * FROM $dbtable ".$where."   ORDER BY `ID` DESC LIMIT ".($page-1)*$maxnewslines.",$maxnewslines;";
		//echo $query;
		$res = mysql_query ($query, $mysql);
		$numlines = sizeof($res);

		if (false === strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'google')){
			$svrlnk = 'http://'.$servername.'/news/index.php?page=1';
		} else {
			$svrlnk = 'http://google.com/';
		}

		// items
		$items = '<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">

<channel>
	<atom:link href="http://'.$servername.'/foreignfiction/rss/rss.php" rel="alternate" type="application/rss+xml" title="Library Genesis: Foreignfiction: News" />	
	<title>Library Genesis: Foreignfiction: News</title>
	<link>'.$svrlnk.'</link>
	<description>Library Genesis: Foreignfiction: News</description>

';

		while($row = mysql_fetch_array($res))
		{

			$id = htmlspecialchars(trim($row['ID']), ENT_QUOTES);
			$md5 = htmlspecialchars(trim($row['MD5']), ENT_QUOTES);
//echo $md5;

			$title = htmlspecialchars(trim($row['Title']), ENT_QUOTES);

			$AuthorFamily1  = htmlspecialchars($row['AuthorFamily1'], ENT_QUOTES);
			$AuthorName1    = htmlspecialchars($row['AuthorName1'], ENT_QUOTES);
			$AuthorSurname1 = htmlspecialchars($row['AuthorSurname1'], ENT_QUOTES);
			$AuthorFamily2  = htmlspecialchars($row['AuthorFamily2'], ENT_QUOTES);
			$AuthorName2    = htmlspecialchars($row['AuthorName2'], ENT_QUOTES);
			$AuthorSurname2 = htmlspecialchars($row['AuthorSurname2'], ENT_QUOTES);
			$AuthorFamily3  = htmlspecialchars($row['AuthorFamily3'], ENT_QUOTES);
			$AuthorName3    = htmlspecialchars($row['AuthorName3'], ENT_QUOTES);
			$AuthorSurname3 = htmlspecialchars($row['AuthorSurname3'], ENT_QUOTES);
			$AuthorFamily4  = htmlspecialchars($row['AuthorFamily4'], ENT_QUOTES);
			$AuthorName4    = htmlspecialchars($row['AuthorName4'], ENT_QUOTES);
			$AuthorSurname4 = htmlspecialchars($row['AuthorSurname4'], ENT_QUOTES);

			$Auth1 = $AuthorFamily1 . ', ' . $AuthorName1 . ' ' . $AuthorSurname1;
			$Auth2 = $AuthorFamily2 . ', ' . $AuthorName2 . ' ' . $AuthorSurname2;
			$Auth3 = $AuthorFamily3 . ', ' . $AuthorName3 . ' ' . $AuthorSurname3;
			$Auth4 = $AuthorFamily4 . ', ' . $AuthorName4 . ' ' . $AuthorSurname4;
			$author     = array_filter(array(
				1 => $Auth1,
				2 => $Auth2,
				3 => $Auth3,
				4 => $Auth4
			));
			$author     = trim(join('; ', $author), ';, ');

			$Series1        = htmlspecialchars($row['Series1'], ENT_QUOTES);
			$Series2        = htmlspecialchars($row['Series2'], ENT_QUOTES);
			$Series3        = htmlspecialchars($row['Series3'], ENT_QUOTES);
			$Series4        = htmlspecialchars($row['Series4'], ENT_QUOTES);

			$series     = array_filter(array(
				1 => $Series1,
				2 => $Series2,
				3 => $Series3,
				4 => $Series4
			));

			$series     = trim(join(';', $series), ';, ');

			$publisher = htmlspecialchars(trim($row['Publisher']), ENT_QUOTES);
			$year = htmlspecialchars(trim($row['Year']), ENT_QUOTES);
			$pages = htmlspecialchars(trim($row['Pages']), ENT_QUOTES);

			$lang = htmlspecialchars(trim($row['Language']), ENT_QUOTES);
			$ident = htmlspecialchars(trim($row['Identifier']), ENT_QUOTES);
			$edition = htmlspecialchars(trim($row['Edition']), ENT_QUOTES);
			$ext = htmlspecialchars(trim($row['Extension']), ENT_QUOTES);


			$coverurl = htmlspecialchars(trim($row['Cover']), ENT_QUOTES);
			if ($coverurl == '0')
			{
				$coverurl = 'http://'.$servername.'/img/blank.png';
			}
			else
			{
				$coverurl = 'http://'.$servername.'/foreignfiction/fiction/covers/' . substr($id, 0, -3) . '000/' . $md5 . '.jpg';
			}
	

			$timeadded = $row['TimeAdded'];
			$size = $row['Filesize'];


			if ($size >= 1024*1024*1024){
				$size = round($size/1024/1024/1024);
				$size = $size.' GB';
			} else
			if ($size >= 1024*1024){
				$size = round($size/1024/1024);
				$size = $size.' MB';
			} else
			if ($size >= 1024){
				$size = round($size/1024);
				$size = $size.' kB';
			} 
			else
			{
				$size = $size.' B';
			}



			if ($series != '') 
			{
				$booknameseries1 = '('.$series.') ';
				$booknameseries2 = '<font color="green"><i>('.$series.')</i></font> ';
                        } 
			else
			{
				$booknameseries1 = '';
				$booknameseries2 = '';
			}

                        $bookname1 = $booknameseries1.$title; //для ссылки
                        $bookname2 = $booknameseries2.$title; //для описания
			unset($booknameseries1); 
			unset($booknameseries2);



			///////////
			// book info section (in parentheses)
			$volinf = $publisher;

			if ($volinf)
			{
				if ($year) 
				{
					$volinf = $volinf.', '.$year;
				}
			}
			else 
			{
				if ($year) $volinf = $year;
			}


			$pp = ' '.$str_pp_en;

			if ($volinf)
			{
				if ($pages) 
				{
					$volinf = $volinf.', '.$pages.$pp;
				} 
				else 
				{
					if ($pages)
					{ 
						$volinf = $pages.$pp;
					}
				}
			}

			///////////


			$ed = ' '.$str_edition_en;
			$vol_ed = '';
			if ($vol_ed)
			{
				if ($edition) 
				{
					$vol_ed = $vol_ed.', '.$edition.$ed;
				}
			} 
			else 
			{
				if ($edition)
				{
					$vol_ed = $edition.$ed;
				}
			}


			if ($vol_ed) 
			{
				$volume = ' <font face="Times" color="green"><i>['.$vol_ed.']</i></font>';
			}

			$volstamp = '';
			if ($volinf) 
			{
				$volstamp = ' <font face="Times" color="green"><i>('.$volinf.')</i></font>';
			}

			
			if (false === strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'google'))
			{
				$link = htmlspecialchars(trim('http://'.$servername.'/foreignfiction/get.php?md5='.$row['MD5']));
			} 
			else 
			{
				$link = 'http://google.com/';
			}


			$descr = '<table border="0"><tr>
			<td rowspan=9 width=120><a href="'.$link.'"><img src="'.$coverurl.'" width="120"></a></td>
			<td valign="top" colspan="2">'.$author.'. <b>'.$bookname2.'</b>'.$volstamp.'</td></tr>
			<tr><td valign="top" width="160"><font color="grey">Author:</font></td><td>'.$author.'</td></tr>
			<tr><td valign="top"><font color="grey">ISBN:</font></td><td>'.$ident.'</td></tr>
			<tr><td valign="top"><font color="grey">Size:</font></td><td>'.$size.' ['.$ext.']</td></tr>
			<tr><td valign="top"><font color="grey">Series:</font></td><td>'.$series.'</td></tr>
			<tr><td valign="top"><font color="grey">Language:</font></td><td>'.$lang.'</td></tr>
			<tr><td valign="top"><font color="grey">ID:</font></td><td>'.$id.'</td></tr>
			<tr><td valign="top"><font color="grey">Date Added:</font></td><td>'.$timeadded.'</td></tr>
			</table>
		';

			$items .= '	<item>
		<guid isPermaLink="false">'.$row['MD5'].'</guid>
		<title>'.$bookname1.'</title>
		<link>'.$link.'</link>
		<description>'.htmlspecialchars($descr).'</description>
	</item>';
unset ($bookname1); unset ($bookname2);
		}
		$items .= '</channel>
</rss>';
		return $items;
	}

}

?>
