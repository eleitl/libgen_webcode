<?php 
define('DB_HOST', '127.0.0.1');
define('DB_USERNAME', '');
define('DB_PASSWORD', '');
define('DB_NAME', '');

$maxnewslines = 30;
$pagesperpage = 25;
$servername = 'libgen.io';
$dbtable = 'main';

$mirrors = array(
	array(
		'title' => 'Gen.lib.rus.ec',
		'url' => 'http://93.174.95.29/fiction/{MD5_uc}'
	),
	array(
		'title' => 'Libgen.lc',
		'url' => 'http://libgen.lc/foreignfiction/ads.php?md5={MD5_uc}'
	),
	array(
		'title' => 'Z-Library',
		'url' => 'http://b-ok.cc/md5/{MD5_uc}'
	),
	array(
		'title' => 'Libgen.me',
		'url' => 'http://fiction.libgen.me/item/detail/{MD5_lc}'
	),
);

$mysql = mysql_connect(DB_HOST, DB_USERNAME, DB_PASSWORD);
if (!$mysql)
{
	error_log(mysql_error());
	http_response_code(500);
	exit();
}
mysql_query("SET NAMES 'utf8'");
mysql_select_db(DB_NAME, $mysql);
