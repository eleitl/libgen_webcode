<?php
include_once '../html.php';
include_once 'config.php';
echo $htmlhead;
// Установка куки для запоминания выбора языка
if (isset($_COOKIE['lang']))
{
	$lang      = $_COOKIE['lang'];
	$lang_file = 'lang_' . $lang . '.php';
	if (!file_exists($lang_file))
	{
		$lang_file = 'lang_en.php';
	}
}
else
{
	$lang      = 'en';
	$lang_file = 'lang_en.php';
}
// -- Конец установки куки
include_once '../lang_' . $lang . '.php';
include_once 'menu_' . $lang . '.html';

//функции
function get_var($var, $type, $def_value)
{
	if (isset($_GET[$var]))
		$value = $_GET[$var];
	if (isset($_POST[$var]))
		$value = $_POST[$var];
	//echo "value=$value ";
	if (!isset($value))
		$value = $def_value;
	//echo "value=$value ";
	if ($type == 'int')
	{
		$value = (int) $value;
		if (!preg_match('/^\d+$/', $value))
			$value = $def_value;
	}
	//echo "value=$value ";
	if ($type == 'text')
	{
		$value = htmlspecialchars($value);
		//$value = mysql_real_escape_string($value);
	}
	return $value;
}
function trimarray($trimstrings)
{
	$trimstrings = preg_replace('~[\;|\,|\:|\$|\\\|\/|\@|\#|\№|\-|\ |\.\[|\(|\{|\|]{0,8}$~isU', '', $trimstrings);
	$trimstrings = preg_replace('~^[\;|\,|\:|\$|\\\|\/|\@|\#|\№|\-|\ |\.\]|\)|\}|\|]{0,8}~isU', '', $trimstrings);
	$trimstrings = strtr($trimstrings, array(
					'<' => '&lt;',
					'>' => '&gt;',
					"'" => "’",
					' "' => ' «',
					'" ' => '» ',
					'"' => '’’',
					"\n" => '<br>', "\r" => ''

				));
	return $trimstrings;
}

//проверяем передаваемые параметры
$page          = get_var("page", "int", 1);
$items_on_page = 25;
$s             = get_var('s', 'text', '');

// получаем список языков в базе, только нормализованные
$res_lang = mysql_query("SELECT 'All' UNION (SELECT `language` FROM `main` GROUP BY `language` HAVING `language`<>'' ORDER BY `language` ASC)", $mysql);
while ($row_lang = mysql_fetch_row($res_lang))
	//echo $row_lang[0];
	$lang_array[] = trim($row_lang[0]);

$f_lang = get_var('f_lang', 'text', '');
if (in_array($f_lang, $lang_array) && $f_lang != 'All') 
	$f_lang_sql = " AND `Language`='" . $f_lang . "' ";
else
	$f_lang_sql = " ";
foreach ($lang_array as $lang)
{
	$lang_escaped = htmlspecialchars($lang);
	$form_searchlang[] = "<option value='" . $lang_escaped . "'" . ($lang == $f_lang ? ' selected' : '') . ">" . $lang_escaped . "</option>";
}
$form_searchlang = "<label><b>" . $LANG_MESS_76 . ":</b></label><select name='f_lang' size='1'>".implode("\n",$form_searchlang)."</select><br>";

//получаем расширения в базе
$res_ext  = mysql_query("SELECT 'All' UNION (SELECT LOWER(`extension`) FROM `main` GROUP BY `extension` HAVING `extension`<>'' ORDER BY `extension` ASC)", $mysql);
while ($row_ext = mysql_fetch_row($res_ext))
	$ext_array[] = $row_ext[0];
$f_ext = get_var('f_ext', 'text', '');	 
if (in_array($f_ext, $ext_array) && $f_ext != 'All')
	$f_ext_sql = " AND `Extension`='" . mysql_real_escape_string($f_ext) . "' ";
else
	$f_ext_sql = " ";
foreach ($ext_array as $ext)
{
	$ext_escaped = htmlspecialchars($ext);
	$form_searchextension[] = "<option value='" . $ext_escaped . "'" . ($ext == $f_ext ? ' selected' : '') . ">" . $ext_escaped . "</option>";
}
$form_searchextension = "<label><b>" . $LANG_MESS_12 . ":</b></label><select name='f_ext' size='1'>".implode("\n", $form_searchextension)."</select><br>";

$f_columns     = get_var('f_columns', 'int', 0); if(!in_array($f_columns, array(0,1,2,3))) $f_columns = 0;

	$colcheck0 = ''; 
	$colcheck1 = ''; 
	$colcheck2 = ''; 
	$colcheck3 = ''; 
	    if($f_columns == 0) $colcheck0 = ' selected'; 
	elseif($f_columns == 1) $colcheck1 = ' selected'; 
	elseif($f_columns == 2) $colcheck2 = ' selected'; 
	elseif($f_columns == 3) $colcheck3 = ' selected'; 

$f_columns = strtr($f_columns, array(
0=>"`AuthorFamily1`,`AuthorName1`,`AuthorSurname1`,`AuthorFamily2`,`AuthorName2`,`AuthorSurname2`,`AuthorFamily3`,`AuthorName3`,`AuthorFamily4`,`AuthorName4`,`Title`,`Series1`,`Series2`,`Series3`,`Extension`", 
1=>"`Title`", 
2=>"`AuthorFamily1`,`AuthorName1`,`AuthorSurname1`,`AuthorFamily2`,`AuthorName2`,`AuthorSurname2`,`AuthorFamily3`,`AuthorName3`,`AuthorSurname3`,`AuthorFamily4`,`AuthorName4`,`AuthorSurname4`", 
3=>"`Series1`,`Series2`,`Series3`,`Series4`"
));

$f_columns_sql = $f_columns;


$f_group       = get_var('f_group', 'int', 0);   if(!in_array($f_group, array(0,1))) $f_group = 1;

	$groupcheck = ' ';
	if($f_group == 1) $groupcheck = ' checked=true';

	if($f_group == 0)
	{
		$f_group_sql = " `MD5` ";
		$f_group_sql_1 = " `MD5` ";
	}
	else
	{
		$f_group_sql = " `AuthorFamily1`, `Title`, `Language`,`Series1` ";
		$f_group_sql_1 = " `AuthorFamily1`, `Title`, `Language` ";
	}

//forms


$form_searchfields = "<label><b>" . $LANG_MESS_4 . ":</b></label>
<select name='f_columns' size='1'>
<option value='0'".$colcheck0.">All</option>
<option value='1'".$colcheck1.">Title</option>
<option value='2'".$colcheck2.">Author</option>
<option value='3'".$colcheck3.">Series</option>
</select><br>";


$form_searchgroup = "<b>" . $LANG_MESS_165 . ":</b><input name='f_group'  VALUE=1 type='checkbox' ".$groupcheck." />";

echo 
'<form name="search" method="get">
<table align="center" width=1000 border=0>
	<tr>
		<td colspan="4" align="center"><font color=#A00000><h1><a href="/">Library Genesis:</a> <a href="/foreignfiction">Fiction</a> <sup><font size=4>1M</font></sup></h1></font></td>
	</tr>
	<tr>
		<td colspan="4"><input autofocus type=edit size=120 name="s" value="' . $s . '"><input type="submit" value="' . $LANG_SEARCH_0 . '" onclick="this.disabled="disabled"; document.forms.item(0).submit();"><br>
		<font face=Arial color=gray size=1><a href="batchsearchindex.php">' . $LANG_MESS_0 . '</a></font></td>
	</tr>
	<tr>
		<td>'.$form_searchlang.'</td>
		<td>'.$form_searchfields.'</td>
		<td>'.$form_searchextension.'</td>
		<td>'.$form_searchgroup.'</td>
	</tr>
</table>
</form>';



$s     = preg_replace("/[[:punct:]]+/u", " ", trim($s));
$s     = preg_replace("/[\s]+/u", " ", $s);
$s     = trim($s);
if(isset($_GET['s']) || isset($_GET['md5']) || isset($_GET['last']))
{
	if($s != '')
	{
		$words = split(" ", $s);
		$w     = array();
		foreach ($words as $word) 
		{
			if (preg_match('~^[0-9]{1,3}$~', $word)) {
				array_push($w, "+(" . ltrim($word, "0") . " 0" . ltrim($word, "0") . " 00" . ltrim($word, "0") . " 000" . ltrim($word, "0") . ")");
			} 
			else 
			{
				array_push($w, "+".$word."*");
			}
		}
	
	
		$sql_world  = " AND MATCH(" . $f_columns_sql . ") AGAINST ('" .(join(" ", $w)) . "'  IN BOOLEAN MODE) ";
		$sql = "SELECT CONCAT(" . $f_group_sql . "), `AuthorFamily1`, `AuthorName1`,  `AuthorSurname1`, 
		`AuthorFamily2`, `AuthorName2`, `AuthorSurname2`, `AuthorFamily3`, `AuthorName3`, `AuthorSurname3`, `AuthorFamily4`, 
 `AuthorName4`, `AuthorSurname4`, `Title`, `Series1`, `Series2`, `Series3`, `Series4`, `Language`,  
 GROUP_CONCAT(MD5 ORDER BY Series1 SEPARATOR ',') AS `md5array` FROM `main` WHERE `title` = '' " . $sql_world . $f_lang_sql . $f_ext_sql . " 
 GROUP BY CONCAT(" . $f_group_sql . ") 

union all

SELECT CONCAT(" . $f_group_sql_1 . "), `AuthorFamily1`, `AuthorName1`, `AuthorSurname1`, 
		`AuthorFamily2`, `AuthorName2`, `AuthorSurname2`, `AuthorFamily3`, `AuthorName3`, `AuthorSurname3`, `AuthorFamily4`, 
 `AuthorName4`, `AuthorSurname4`, `Title`, `Series1`, `Series2`, `Series3`, `Series4`, `Language`,  
 GROUP_CONCAT(MD5 ORDER BY Series1 desc SEPARATOR ',') AS `md5array` FROM `main` WHERE `title` != ''  " . $sql_world . $f_lang_sql . $f_ext_sql . " 
 GROUP BY CONCAT(" . $f_group_sql_1 . ") 

ORDER BY `language`, `title`,LENGTH(`series1`), `series1` ";
	}
	elseif(isset($_GET['md5']))
	{
		if(preg_match('|^[0-9A-Fa-f]{32}$|', $_GET['md5']))
		{
			$sql = "SELECT `AuthorFamily1`, `AuthorName1`, `AuthorSurname1`, `AuthorFamily2`, `AuthorName2`, `AuthorSurname2`, `AuthorFamily3`, `AuthorName3`, `AuthorSurname3`, `AuthorFamily4`, `AuthorName4`, `AuthorSurname4`, `Title`, `Series1`, `Series2`, `Series3`, `Series4`, `Language`, `MD5` as `md5array` FROM `main` WHERE `MD5` = '".$_GET['md5']."'";
		}
	}
	elseif(isset($_GET['last']))
	{
		$f_lang_sql = str_replace('AND', '', $f_lang_sql);
		$sql = 	"SELECT SQL_CACHE `AuthorFamily1`, `AuthorName1`, `AuthorSurname1`, `AuthorFamily2`, `AuthorName2`, `AuthorSurname2`, `AuthorFamily3`, `AuthorName3`, `AuthorSurname3`, `AuthorFamily4`, `AuthorName4`, `AuthorSurname4`, `Title`, `Series1`, `Series2`, `Series3`, `Series4`, `Language`,  `MD5` AS `md5array` FROM `main`
		 WHERE 1=1 ".$f_lang_sql. $f_ext_sql."  ORDER BY `ID` DESC LIMIT 100000";
	}
	elseif($s == '' && ($_GET['f_lang'] !='' || $_GET['f_ext'] !='' ))
	{
		$sql = 	"SELECT SQL_CACHE `AuthorFamily1`, `AuthorName1`, `AuthorSurname1`, `AuthorFamily2`, `AuthorName2`, `AuthorSurname2`, `AuthorFamily3`, `AuthorName3`, `AuthorSurname3`, `AuthorFamily4`, `AuthorName4`, `AuthorSurname4`, `Title`, `Series1`, `Series2`, `Series3`, `Series4`, `Language`,  `MD5` AS `md5array` FROM `main`
		 WHERE 1=1 " . $f_lang_sql . $f_ext_sql . "  ORDER BY `ID` DESC LIMIT 100000";
	}
	else
	{
		$sql = 	"SELECT SQL_CACHE `AuthorFamily1`, `AuthorName1`, `AuthorSurname1`, `AuthorFamily2`, `AuthorName2`, `AuthorSurname2`, `AuthorFamily3`, `AuthorName3`, `AuthorSurname3`, `AuthorFamily4`, `AuthorName4`, `AuthorSurname4`, `Title`, `Series1`, `Series2`, `Series3`, `Series4`, `Language`,  `MD5` AS `md5array` FROM `main`
		 WHERE 1=2 ";
	}	

	$res   = mysql_query($sql, $mysql); //echo $sql." ".mysql_error()."<br>\n";
	$cn    = mysql_num_rows($res);
	$pages = ceil($cn / $items_on_page);

	$nav   = "";
	if (isset($_GET['md5']))
	{
	  	$nav   = "";
	}
	elseif(isset($_GET['last']))
	{
		$nav = 'index.php?last=&f_ext=' . $_GET['f_ext'] . '&f_lang=' . $_GET['f_lang'] . '&page=';
	}
	else	
	{
		$nav   = 'index.php?s=' . $s . '&f_columns=' . $_GET['f_columns'] . '&f_lang=' . $_GET['f_lang'] . '&f_group=' . $_GET['f_group'] . '&f_ext=' . $_GET['f_ext'] .'&page=';
	}
	


	if ($cn == 100000) echo '<font color=grey size=1>' . $LANG_MESS_232 . '  ' . $cn . ' '. $LANG_MESS_190 .' </font>'; else echo '<font color=grey size=1>' . $LANG_MESS_77 . ' ' . $cn . ' '. $LANG_MESS_190 .' </font>';


	if ($pages > 1)
	{

	$to_page = ($page * 25); if($to_page > $cn) $to_page = $cn;
	echo '<font color=grey size=1>| ' . $LANG_MESS_231 . ' ' .(($page * 25) -24 ) . ' '.$LANG_MESS_63.' ' .$to_page. '</font>';
	echo '<div style="text-align: center;" class="paginator" id="paginator_example_top"></div>
	<script type="text/javascript">
	    paginator_example_top = new Paginator(
	        "paginator_example_top", // id контейнера, куда ляжет пагинатор
	        ' . $pages . ', // общее число страниц
	        25, // число страниц, видимых одновременно
	        ' . $page . ', // номер текущей страницы
	        "' . $nav . '" // url страниц
	    );
	</script>';
	}
	$i         = 0;
	$links     = "";
	echo <<<HTML
<table cellspacing=1 cellpadding=1 rules=rows align=center>
<tr>
	<td width=200><b>{$LANG_MESS_6}</b></td>
	<td width=150><b>{$LANG_MESS_7}</b></td>
	<td width=400><b>{$LANG_MESS_5}</b></td>
	<td width=70><b>{$LANG_MESS_11}</b></td>
	<td width=280><b>{$LANG_MESS_75}</b></td>
</tr>
HTML;
	while ($row = mysql_fetch_assoc($res))
	{
		if ($i >= (($page - 1) * $items_on_page) and ($i < $page * $items_on_page))
		{
			$Title          = trimarray(htmlspecialchars($row['Title'], ENT_QUOTES));
			$AuthorFamily1  = htmlspecialchars($row['AuthorFamily1'], ENT_QUOTES);
			$AuthorName1    = htmlspecialchars($row['AuthorName1'], ENT_QUOTES);
			$AuthorSurname1 = htmlspecialchars($row['AuthorSurname1'], ENT_QUOTES);
			$AuthorFamily2  = htmlspecialchars($row['AuthorFamily2'], ENT_QUOTES);
			$AuthorName2    = htmlspecialchars($row['AuthorName2'], ENT_QUOTES);
			$AuthorSurname2 = htmlspecialchars($row['AuthorSurname2'], ENT_QUOTES);
			$AuthorFamily3  = htmlspecialchars($row['AuthorFamily3'], ENT_QUOTES);
			$AuthorName3    = htmlspecialchars($row['AuthorName3'], ENT_QUOTES);
			$AuthorSurname3 = htmlspecialchars($row['AuthorSurname3'], ENT_QUOTES);
			$AuthorFamily4  = htmlspecialchars($row['AuthorFamily4'], ENT_QUOTES);
			$AuthorName4    = htmlspecialchars($row['AuthorName4'], ENT_QUOTES);
			$AuthorSurname4 = htmlspecialchars($row['AuthorSurname4'], ENT_QUOTES);
			$Language       = htmlspecialchars(trimarray($row['Language']), ENT_QUOTES);
			$Series1        = htmlspecialchars(trimarray($row['Series1']), ENT_QUOTES);
			$Series2        = htmlspecialchars(trimarray($row['Series2']), ENT_QUOTES);
			$Series3        = htmlspecialchars(trimarray($row['Series3']), ENT_QUOTES);
			$Series4        = htmlspecialchars(trimarray($row['Series4']), ENT_QUOTES);
			$md5array       = htmlspecialchars($row['md5array'], ENT_QUOTES);
			foreach (explode(',', $md5array) as $hash)
			{
				$resforeach       = mysql_query("SELECT * FROM main WHERE `md5` = '$hash'");
				$rowforeach       = mysql_fetch_assoc($resforeach);
				$titleforeach     = trimarray($rowforeach['Title']);
				$idforeach        = stripslashes($rowforeach['ID']);
				$extensionforeach = stripslashes($rowforeach['Extension']);
				$md5foreach       = stripslashes($rowforeach['MD5']);
				$filesizeforeach  = $rowforeach['Filesize'];
				if ($filesizeforeach >= 1024 * 1024 * 1024)
				{
					$filesizeforeach = round($filesizeforeach / 1024 / 1024 / 1024);
					$filesizeforeach = $filesizeforeach . '&nbsp;' . $LANG_MESS_GB;
				}
				else if ($filesizeforeach >= 1024 * 1024)
				{
					$filesizeforeach = round($filesizeforeach / 1024 / 1024);
					$filesizeforeach = $filesizeforeach . '&nbsp;' . $LANG_MESS_MB;
				}
				else if ($filesizeforeach >= 1024)
				{
					$filesizeforeach = round($filesizeforeach / 1024);
					$filesizeforeach = $filesizeforeach . '&nbsp;' . $LANG_MESS_KB;
				}
				else
					$filesizeforeach = $filesizeforeach . '&nbsp;' . $LANG_MESS_B;
				$authorfamily1foreach  = htmlspecialchars($rowforeach['AuthorFamily1'], ENT_QUOTES);
				$authorfamily2foreach  = htmlspecialchars($rowforeach['AuthorFamily2'], ENT_QUOTES);
				$authorfamily3foreach  = htmlspecialchars($rowforeach['AuthorFamily3'], ENT_QUOTES);
				$authorfamily4foreach  = htmlspecialchars($rowforeach['AuthorFamily4'], ENT_QUOTES);
				$authorname1foreach    = htmlspecialchars($rowforeach['AuthorName1'], ENT_QUOTES);
				$authorname2foreach    = htmlspecialchars($rowforeach['AuthorName2'], ENT_QUOTES);
				$authorname3foreach    = htmlspecialchars($rowforeach['AuthorName3'], ENT_QUOTES);
				$authorname4foreach    = htmlspecialchars($rowforeach['AuthorName4'], ENT_QUOTES);
				$authorsurname1foreach = htmlspecialchars($rowforeach['AuthorSurname1'], ENT_QUOTES);
				$authorsurname2foreach = htmlspecialchars($rowforeach['AuthorSurname2'], ENT_QUOTES);
				$authorsurname3foreach = htmlspecialchars($rowforeach['AuthorSurname3'], ENT_QUOTES);
				$authorsurname4foreach = htmlspecialchars($rowforeach['AuthorSurname4'], ENT_QUOTES);
				$author1foreach        = trimarray($authorfamily1foreach . ', ' . $authorname1foreach . ' ' . $authorsurname1foreach);
				$author2foreach        = trimarray($authorfamily2foreach . ', ' . $authorname2foreach . ' ' . $authorsurname2foreach);
				$author3foreach        = trimarray($authorfamily3foreach . ', ' . $authorname3foreach . ' ' . $authorsurname3foreach);
				$author4foreach        = trimarray($authorfamily4foreach . ', ' . $authorname4foreach . ' ' . $authorsurname4foreach);
				$series1foreach        = htmlspecialchars(trimarray($rowforeach['Series1']), ENT_QUOTES);
				$series2foreach        = htmlspecialchars(trimarray($rowforeach['Series2']), ENT_QUOTES);
				$series3foreach        = htmlspecialchars(trimarray($rowforeach['Series3']), ENT_QUOTES);
				$series4foreach        = htmlspecialchars(trimarray($rowforeach['Series4']), ENT_QUOTES);
				$seriesallforeach      = trim($series1foreach . ', ' . $series2foreach . ', ' . $series3foreach . ', ' . $series4foreach, ' ,-');
				$languageforeach       = htmlspecialchars($rowforeach['Language'], ENT_QUOTES);
				$pagesforeach          = htmlspecialchars($rowforeach['Pages'], ENT_QUOTES);
				$timeforeach           = htmlspecialchars($rowforeach['TimeAdded'], ENT_QUOTES);
				$identifierforeach     = htmlspecialchars($rowforeach['Identifier'], ENT_QUOTES);
				$commentaryforeach     = strtr($rowforeach['Commentary'], array(
					'<' => '&lt;',
					'>' => '&gt;',
					"'" => "’",
					' "' => ' «',
					'" ' => '» ',
					'"' => '’’',
					"\n" => '<br>',
					"\r" => ''
				));
				$yearforeach           = htmlspecialchars(trimarray($rowforeach['Year']), ENT_QUOTES);
				$publisherforeach      = htmlspecialchars(trimarray($rowforeach['Publisher']), ENT_QUOTES);
				if (stripslashes($rowforeach['Cover']) == '0')
					$coverforeach = '../img/blank.png';
				else
					$coverforeach = '/fictioncovers/' . substr($idforeach, 0, -3) . '000/' . $hash . '.jpg';

				$wind[] = '<div style="display:inline" onmouseover="AddTT(\'<table width=600 border = 0 rules = rows><tr><td  ROWSPAN=12><img src=' . $coverforeach . ' height=300></td><td>Title:</td><td>' . $titleforeach . '</td></tr><tr><td>Author1:</td><td>' . $author1foreach . '</td></tr><tr><td>Author2:</td><td>' . $author2foreach . '</td></tr><tr><td>Author3:</td><td>' . $author3foreach . '</td></tr><tr><td>Author4:</td><td>' . $author4foreach . '</td></tr><tr><td>Series:</td><td>' . $seriesallforeach . '</td></tr><tr><td>Year:</td><td>' . $yearforeach . '</td></tr><tr><td>Publisher:</td><td>' . $publisherforeach . '</td></tr><tr><td>Identifier:</td><td>' . $identifierforeach . '</td></tr><tr><td>Pages:</td><td>' . $pagesforeach . '</td></tr><tr><td>Language:</td><td>' . $languageforeach . '</td></tr><tr><td></td></tr><tr><td ROWSPAN=2 COLSPAN=3><b>Commentary:</b>' . $commentaryforeach . '</td></tr></table>\');" onmouseout="RemoveTT();" title="Libgen ID: ' . $idforeach . '; Added: ' . $timeforeach . '">' .	strtoupper($extensionforeach) . ' (' . $filesizeforeach . ')</div>: ' . download_links($hash);
			}
			$windall = implode('<br>', $wind);
			unset($wind);
			$Auth1 = trimarray($AuthorFamily1 . ', ' . $AuthorName1 . ' ' . $AuthorSurname1);
			$Auth2 = trimarray($AuthorFamily2 . ', ' . $AuthorName2 . ' ' . $AuthorSurname2);
			$Auth3 = trimarray($AuthorFamily3 . ', ' . $AuthorName3 . ' ' . $AuthorSurname3);
			$Auth4 = trimarray($AuthorFamily4 . ', ' . $AuthorName4 . ' ' . $AuthorSurname4);
			$a     = array_filter(array(
				1 => $Auth1,
				2 => $Auth2,
				3 => $Auth3,
				4 => $Auth4
			));

			foreach ($a as $a1)
				$a2[] = '<a href="/foreignfiction/index.php?s=' . rawurldecode($a1) . '&f_lang=0&f_columns=2&f_group=1">' . $a1 . '</a>';
			echo '<tr><td width=200>' . join('<br>', $a2) . '</td><td width=150>' . preg_replace('~(<br>){1,3}$~', '', $Series1 . '<br>' . $Series2 . '<br>' . $Series3 . '<br>' . $Series4) . '</td><td width=400>' . $Title . '</td><td width=70>' . $Language . '</td><td width=280>' . $windall . '</td></tr>';
			$links .= stripslashes($row['Title']) . "\n";
			unset($a1, $a2);
		}
		$i++;
	}
	echo "</table>";

	if ($pages > 1)
	{

	echo '<div style="text-align: center;" class="paginator" id="paginator_example_bottom"></div>
	<script type="text/javascript">
	    paginator_example_bottom = new Paginator(
	        "paginator_example_bottom", // id контейнера, куда ляжет пагинатор
	        ' . $pages . ', // общее число страниц
	        25, // число страниц, видимых одновременно
	        ' . $page . ', // номер текущей страницы
	        "' . $nav . '" // url страниц
	    );
	</script>';
	}
}
echo $htmlfoot;
mysql_free_result($res);
mysql_close($mysql);

function download_links($hash)
{
	global $mirrors;
	$links = array();
	foreach ($mirrors as $i => $mirror)
	{
		$mirror['url'] = str_replace('{MD5_lc}', strtolower($hash), $mirror['url']);
		$mirror['url'] = str_replace('{MD5_uc}', strtoupper($hash), $mirror['url']);
		$links[] = '<a href="' . $mirror['url'] . '" title="' . htmlspecialchars($mirror['title']) . '">[' . ($i + 1) . ']</a>';
	}
	return implode(' ', $links);
}
